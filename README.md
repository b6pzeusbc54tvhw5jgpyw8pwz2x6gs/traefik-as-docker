# traefik uging docker-compose

## preparation

copy your certification key, crt file to certs directory as `traefik.key`, `traefik.crt`

And copy example configure file to `traefik.toml` file name
```
$ cp traefik-example.toml traefik.toml
```

## start

```
$ docker-compose up -d
```

## test
To access traefik dashboard Open "https://traefik.alfreduc.com" in your browser.
> change to `alfreduc.com` to your own domain.

- To connect other docker container through this traefik, refer follow:
https://gitlab.com/b6pzeusbc54tvhw5jgpyw8pwz2x6gs/nginx-behind-traefik

## log
```
$ tail -f ./log/access.log
$ tail -f ./log/traefik.log
```

## reference
Official website: https://traefik.io/

